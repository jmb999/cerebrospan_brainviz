### do_cerebroViz_plots.R ###
# Overview: Takes a user-supplied list of gene/transcript IDs, loads a PsychENCODE expression data matrix,
# extracts the relevant BrainSpan data points, and generates a series of CerebroViz plots. 

# Input:
#  -A list of IDs to plot (tab-delimited file with IDs in the first column). If transcript IDs are given in tthe format ENSTxxxx.yy, the '.yy' portion will be stripped out
#  -Flag --i specifying whether the ID list passed in the first positional argument contains (i)soform IDs or gene IDs (which is the default). Toggles between 2 Rdata workspaces containing expression matrices
#  -PsychENCODE metadata file (hardcoded)
#  -Table listing recognized CerebroViz brain regions and their BrainSpan abbreviation equivalents (hardcoded)
#  -Path to a folder where results will be written

# Output (for each ENST/ENSG present in the expression matrix):
#  -Data matrix describing the median expression of the feature across regions/developmental periods
#  -CerebroViz plot showing the median expression across all timepoints (svg)
#  -CerebroViz plots showing the median expression at each timepoint (svg)
#  -Expression heatmap (jpg)

# Add local .Rlib to path before loading cerebroViz
.libPaths( c( .libPaths(), "/hpc/users/belmoj01/.Rlib") )
library(cerebroViz)
library(RColorBrewer)
library(stringr)
library(argparse)

### Read & process command line args

parser = ArgumentParser(description='Overview: Takes a user-supplied list of ENSEMBL gene/transcript IDs, loads a PsychENCODE expression data matrix, extracts the relevant BrainSpan data points, and generates a series of CerebroViz plots')
parser$add_argument('idlistpath', metavar='path_to_id_list', type='character', nargs=1, action='store', help='Path to a list of ENST/ENSG IDs to plot in CerebroViz, one ID per line. If file has multiple columns, the first column must contain the IDs.')
parser$add_argument('brainregionpath', metavar='path_to_brain_region_file', type='character', nargs=1, action='store', default='addtl_files/brain_regions.csv', help='Path to file containing map of Cerebroviz to BrainSpan regions')
parser$add_argument('metadatapath', metavar='path_to_metadata_file', type='character', nargs=1, action='store', default='addtl_files/metaData_RNAseqFreeze1+2.csv', help='Path to .csv metadata file for PsychENCODE data')
parser$add_argument('output_dir', metavar='output_path', type='character', nargs=1, action='store', help='Base path where results will be written')
parser$add_argument('-i,--isoforms', dest='useIsos', action='store_true', default=FALSE, help='Use the PsychENCODE data matrix containing isoform-level expression data. This flag should be set when the 1st argument (path_to_id_list) is a list of ENSEMBL transcript IDs. When this flag is supplied the data matrix used is \'/sc/arion/projects/EPIASD/PsychEncodeCrossDisorderCaptsone_dropbox/isoform-expression_final/Isoform.TPM.noOutliersamples.AllIsoforms.RData\'. Otherwise, the default workspace used is \'/sc/arion/projects/EPIASD/PsychEncodeCrossDisorderCapstone/working_data/summarizedExperiment/se.BrainSpan.RData\'.')
args = parser$parse_args()

idlistpath = args$idlistpath
brainregionpath = args$brainregionpath
metadatapath = args$metadatapath
output_basedir = args$output_dir
useIsos = args$useIsos

### Read input files

#idlist = as.character(read.table('/hpc/users/belmoj01/CHD8isoform_ENST.csv')[,1])
idlist = as.character(read.table(idlistpath, sep='\t')[,1])
idlist = unlist(lapply(idlist, function(x){return(unlist(strsplit(x,'\\.'))[1])}))

#output_basedir = '/hpc/users/belmoj01/cviz_input_matrices/noOutliers/'

# Load necessary workspaces/read files
if(useIsos){
 # Load the isoform workspace
 load('/sc/arion/projects/EPIASD/PsychEncodeCrossDisorderCaptsone_dropbox/isoform-expression_final/Isoform.TPM.noOutliersamples.AllIsoforms.RData')
 bspancolpattern = "BrainSpan_(HSB[0-9]*)_([a-zA-Z0-9]+)"
} else {
 # Alternatively, load the 'all BSpan' workspace
 load('/sc/arion/projects/EPIASD/PsychEncodeCrossDisorderCapstone/working_data/summarizedExperiment/se.BrainSpan.RData')
 bspancolpattern = "(HSB[0-9]*)_([a-zA-Z0-9]+)"
 tpm = se.BrainSpan@assays[['tpm']]
}

meta = read.table(metadatapath, sep=',',header=TRUE)

cviztable = read.table(brainregionpath,sep=',',header=TRUE)

########

# Prepare metadata

meta = meta[meta['study']=='BrainSpan',]
rownames(meta) = meta[,'specimenID']

# Prepare expression data
dat = do.call(rbind, lapply(colnames(tpm), str_match, bspancolpattern))
dat = dat[!is.na(dat[,1]),]
colnames(dat) = c('tpmID','sample','region')
dat = as.data.frame(dat)
dat[,'specimenID'] = paste(dat[,'sample'],dat[,'region'],sep='_')

# Re-map region IDs from BrainSpan -> CerebroViz
levels(dat$region) = c(levels(dat$region), 'CB', 'THA') # Add 'CBC' and 'THA' to levels for this column
dat[dat$region=='CBC','region'] = 'CB' 
dat[dat$region=='MD','region'] = 'THA' 

#cviztable[cviztable[,'cerebroViz']=='CB','cerebroViz'] = 'CBC'
#cviztable[cviztable[,'cerebroViz']=='MD','cerebroViz'] = 'THA'

# Function to assign age bin
assignAgeBin = function(x){
  age = as.numeric(x)
  if(age >= 28 & age < 56){return ('4-7 pcw')}
  else if(age >= 56 & age < 70){return ('8-9 pcw')}
  else if(age >= 70 & age < 91){return ('10-12 pcw')}
  else if(age >= 91 & age < 112){return ('13-15 pcw')}
  else if(age >= 112 & age < 133){return ('16-18 pcw')}
  else if(age >= 133 & age < 175){return ('19-24 pcw')}
  else if(age >= 175 & age < 270){return ('25-38 pcw')}
  else if(age >= 270 & age < 450){return ('Birth-5 mos')}
  else if(age >= 450 & age < 840){return ('6-19 mos')}
  else if(age >= 840 & age < 2460){return ('19 mos-5 yrs')}
  else if(age >= 2460 & age < 4650){return ('6-11 yrs')}
  else if(age >= 4650 & age < 7570){return ('12-20 yrs')}
  else if(age >= 7570 & age < 22170){return ('20-60+ yrs')}
  else {return ('???? yrs')}
}

meta$ageBin = unlist(lapply(meta$ageDaysPostIncep, assignAgeBin))

# Add ageBin column to data frame
rownames(dat) = dat$specimenID
dat$ageBin = unlist(lapply(rownames(dat),function(x){meta[x,'ageBin']}))

# Initialize empty data frame that will hold combined results for ALL genes (regions combined)
devorder = c('4-7 pcw','8-9 pcw','10-12 pcw','13-15 pcw','16-18 pcw','19-24 pcw','25-38 pcw','Birth-5 mos','6-19 mos','19 mos-5 yrs','6-11 yrs','12-20 yrs','20-60+ yrs')
genedevdf=data.frame(matrix(ncol = length(devorder), nrow = length(idlist)))
rownames(genedevdf) = idlist
colnames(genedevdf) = devorder

# Iterate over each ID in the user-supplied list and generate the region-specific expression profiles & plots 
regions = as.character(cviztable$cerebroViz)
for(enst in idlist){
  
  if(!(enst %in% rownames(tpm))){
    print(paste(enst,' is not found in the Expression Matrix. Skipping.', sep=''))
    next
  }

  output_dir = paste(output_basedir,enst,'/',sep='')
  if (!file.exists(output_dir)){
    dir.create(file.path(output_basedir, enst))
  }

  df = data.frame()
  for(r in regions){
    for(t in dat[dat$region==r,'ageBin']){
      tpmIDs = dat[dat$region==r & dat$ageBin==t,'tpmID']
      a = tpm[enst,as.character(tpmIDs)]
      df[r,t] = median(unlist(tpm[enst,as.character(tpmIDs)]))
    }
  }
  # Reorder columns by ascending developmental window
  colorder = match(devorder, colnames(df))
  df = df[,devorder[!is.na(colorder)]]
  write.table(df, paste(output_dir,enst,'_cerebroViz.mat',sep=''), sep='\t', row.names=TRUE, col.names=TRUE, quote=FALSE)

  # Take average across all brain regions by period and add to to data frame 
  # NOTE: Need to set missing columns to NA first or dies with error
  genedevdf[enst,] = apply(df,2,mean,na.rm=T)

  if(ncol(df[,colSums(is.na(df))<nrow(df)])>0){
    ### Plot data in CerebroViz ###
   tryCatch({
   
      cscale_mat = cerebroScale(as.matrix(df), clamp = NULL, divData = FALSE, center_zero = TRUE)

      # Do general plot (all timepoints combined)
      cerebroViz(cscale_mat, filePrefix = paste(output_dir,enst,'_combined',sep=''), naHatch = TRUE, divData = TRUE, timePoint = ncol(cscale_mat), regLabel = TRUE)

      # Do series of individual timepoint plots
      cerebroViz(cscale_mat, filePrefix = paste(output_dir,enst,sep=''), naHatch = TRUE, divData = TRUE, timePoint = 1:ncol(cscale_mat), regLabel = TRUE, figLabel = TRUE)
 
      # Make expression heatmap
      jpeg(file=paste(output_dir,enst,'_cVizHeatmap.jpg',sep=''))
      par(mar=c(1,1,1,1))
      heatmap(cscale_mat, Colv = NA, scale = "none", col = rev(brewer.pal(11, "RdYlBu")))
      dev.off()
    }, error = function(e){print(e)}
    )
  }
}

write.table(genedevdf, file=paste(output_dir,'geneavgexprbyperiod_regioncollapsed.csv',sep=','),quote=F)
