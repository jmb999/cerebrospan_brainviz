# Cerebrospan Brainviz #

Script to visualize a list of Ensembl gene/transcript IDs in cerebroVIZ (https://github.com/ethanbahl/cerebroViz) using data from BrainSpan.

Must be run on on the chimera cluster by a user with access to the project folder /sc/arion/projects/EPIASD/. The paths to the expression matrices are hard-coded.
